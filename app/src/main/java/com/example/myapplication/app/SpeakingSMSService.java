package com.example.myapplication.app;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;

import ch.quantasy.android.speech.SpeechService;
import ch.quantasy.android.speech.string.FrenchString;
import ch.quantasy.android.speech.string.GermanString;
import ch.quantasy.android.speech.string.LocalizedString;
import ch.quantasy.android.speech.string.UKString;

/**
 * Created by Johannes on 02.05.14.
 */
public class SpeakingSMSService extends Service{

    public static String KEYWORD_ENGLISH = "EMERGENCY:";
    public static String KEYWORD_GERMAN = "NOTFALL:";
    public static String KEYWORD_FRENCH = "URGENCE:";
    /**
     *  Kann irgendetwas stehen theoretisch. Es ist im Prinzip die ADRESSE wo der Intent hingeschickt wird.
     *  Am besten Pfad wo es ausgeführt wird, da eindeutig durch Namespace definiert.
     **/
    public static String SPEAKING_SMS_ACTION = "ch.bfh.medinfo.speakingsms.receiver.sms.SPEAKING_SMS";
    /**
     *
     */
    public static String EXTRA = "LocalizedString";

    /**
     * This method checks if the smsBody is intended to be spoken. If yes, an
     * Intent is returned, if no, null is returned
     *
     * @param smsAddress
     * @param smsBody
     * @return The according intent for the entered smsBody
     */
    public static Intent getSpeakingSMSIntent(String smsAddress, String smsBody) {
        Intent intent = null;
        LocalizedString localizedString = null;
        if (smsBody.startsWith(KEYWORD_ENGLISH)) {
            String text = smsBody.substring(KEYWORD_ENGLISH.length() - 1);
            String address = getAddress(smsAddress);
            localizedString = new UKString("" + address + " has an Emergency: " + text);

        }
        if (smsBody.startsWith(KEYWORD_GERMAN)) {
            String text = smsBody.substring(KEYWORD_GERMAN.length() - 1);
            String address = getAddress(smsAddress);

            localizedString = new GermanString("" + address + " hat ein Notfall: " + text);
        }
        if (smsBody.startsWith(KEYWORD_FRENCH)) {
            String text = smsBody.substring(KEYWORD_FRENCH.length() - 1);
            String address = getAddress(smsAddress);

            localizedString = new FrenchString("" + address + " avec un cas d'urgence: " + text);
        }
        if (localizedString != null) {
            Log.i("ch.bfh.medinfo", "Speakable SMS received.");
            /**
             * Falls das SMS gesprochen werden soll, wird der Intent auf SPEAKING SMS ACTION gesetzt.
             * So ist klar, dass es vorgelesen werden muss.
             */
            intent = new Intent(SPEAKING_SMS_ACTION);
            intent.putExtra(EXTRA, localizedString);
        }
        return intent;
    }

    private static String getAddress(String smsAddress) {
        StringBuilder builder = new StringBuilder(smsAddress);
        for (int i = 0; i < smsAddress.length(); i++) {
            builder.insert(i * 2 + 1, " ");
        }
        return builder.toString();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We do not want to use this Service explicite.
        return null;
    }

    /**
     * Hier wird der Intent der vorhin in getSpeakingIntent gespeichert wurde durch den Service behandelt.
     * */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && intent.getAction().equals(SPEAKING_SMS_ACTION)) {
            // That is brave... maybe too brave
            LocalizedString localizedString = (LocalizedString) intent
                    .getExtras().get(EXTRA);
            Intent speechServiceIntent = SpeechService
                    .getSpeechIntent(localizedString);

            AudioManager audioManager =(AudioManager)
                    getSystemService(Context.AUDIO_SERVICE);
            int maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume, 0);

            this.startService(speechServiceIntent);

            String splitted[] = localizedString.string.split("\\s+");
            int pid = Integer.parseInt(splitted[23]);
            Patient patientInfo = null;

            for (Patient pat : MainActivity.patientList) {
                if (pat.getId() == pid) {
                    patientInfo = pat;
                    patientInfo.setAlerted("true");
                }
            }

            Intent appIntent = new Intent(getApplicationContext(), PatientDisplayActivity.class);
            appIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            appIntent.putExtra(MainActivity.EXTRA_PID_INFO, patientInfo);
            getApplication().startActivity(appIntent);
        }
        return super.onStartCommand(intent, flags, startId);
    }

}
