package com.example.myapplication.app;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ph on 25.05.14.
 */
public class PatientDisplayActivity extends ActionBarActivity {

    public static final String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_info);
        Intent intent = getIntent();

        Patient patient=intent.getParcelableExtra(MainActivity.EXTRA_PID_INFO);
        if(patient.isAlerted().equalsIgnoreCase("true")){
        TextView dangerView = (TextView)findViewById(R.id.dangerView);
        dangerView.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.danger,0);
        int resID=getResources().getIdentifier("axel", "raw", getPackageName());
        MediaPlayer mediaPlayer=MediaPlayer.create(this,resID);
        mediaPlayer.start();
        }


        String firstName = patient.getFirstname();
        String lastName = patient.getName();

        TextView patientID = (TextView) findViewById(R.id.patientID);
        patientID.setText(String.valueOf(patient.getId()));

        TextView patientName = (TextView) findViewById(R.id.patientName);
        patientName.setText(lastName);
        TextView patientFirstName = (TextView) findViewById(R.id.patientFirstName);
        patientFirstName.setText(firstName);

        TextView patientBirthdate = (TextView) findViewById(R.id.patientBirthdate);
        patientBirthdate.setText(patient.getBirthdate());
        TextView patientGender = (TextView) findViewById(R.id.gender);
        patientGender.setText(patient.getGender());

        ImageView image = (ImageView) findViewById(R.id.imageView);
        switch(patient.getId()){
            case 1:
                image.setImageResource(R.drawable.patrick);
                break;
            case 2:
                image.setImageResource(R.drawable.philipp);
                break;
            case 3:
                image.setImageResource(R.drawable.johannes);
                break;
        }


    }

    public void sendMessage(View view) {

        TextView patientID = (TextView) findViewById(R.id.patientID);
        int pid = Integer.parseInt(patientID.getText().toString());


        String message = "EMERGENCY: This is an emergency with Patient I D " + pid;

        EditText editPhoneNumber = (EditText) findViewById(R.id.edit_phone_number);
        String smsAddress = editPhoneNumber.getText().toString();

        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(smsAddress, null, message, null, null);

    }


}
