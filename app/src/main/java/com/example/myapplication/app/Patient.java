package com.example.myapplication.app;

import android.graphics.Picture;
import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Johannes on 23.05.14.
 */
public class Patient implements Parcelable {

    public static final Creator<Patient> CREATOR
            = new Parcelable.Creator<Patient>() {
        @Override
        public Patient createFromParcel(Parcel source) {
            return new Patient(source);
        }

        @Override
        public Patient[] newArray(int size) {
            return new Patient[0];
        }
    };

    private int id;
    private String name;
    private String firstname;
    private String pic;
    private String birthdate;
    private String alerted = "false";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    private String gender;

    public Patient(Parcel source) {
        this.setId(source.readInt());
        this.setName(source.readString());
        this.setFirstname(source.readString());
        this.setBirthdate(source.readString());
        this.setGender(source.readString());
        this.setPic(source.readString());
        this.setAlerted(source.readString());
    }

    public Patient(int id, String name, String firstname, String birthdate, String gender) {
        this.id = id;
        this.name = name;
        this.firstname = firstname;
        this.birthdate = birthdate;
        this.gender = gender;
    }

    public Patient(int id, String name, String firstname, String pic, String birthdate, String gender) {
        this(id, name, firstname, birthdate, gender);
        this.pic = pic;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.getId());
        dest.writeString(this.getName());
        dest.writeString(this.getFirstname());
        dest.writeString(this.getBirthdate());
        dest.writeString(this.getGender());
        dest.writeString(this.getPic());
        dest.writeString(this.isAlerted());

    }


    public String isAlerted() {
        return alerted;
    }

    public void setAlerted(String alerted) {
        this.alerted = alerted;
    }
}
