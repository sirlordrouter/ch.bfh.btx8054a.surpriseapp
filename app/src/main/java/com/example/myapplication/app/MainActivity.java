package com.example.myapplication.app;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Arrays;


public class MainActivity extends ActionBarActivity {

    public static final String EXTRA_PID_INFO = "com.example.myfirstapp.PID_INFO";
    private ListView listView ;
    private ArrayAdapter<Patient> listAdapter ;
    public static ArrayList<Patient> patientList = new ArrayList<Patient>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById( R.id.listview );
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent intent = new Intent(MainActivity.this, PatientDisplayActivity.class);

                Patient patient = (Patient)parent.getItemAtPosition(position);

                intent.putExtra(EXTRA_PID_INFO, patient);
                startActivity(intent);

            }
        });
        if(patientList.isEmpty()){
        Patient hirschli = new Patient(1, "Hirschi", "Patrick", "12.01.1989", "male");
        Patient kleinSchaad = new Patient(2, "Schaad", "Philipp", "16.06.1987", "male");
        Patient gnagJ = new Patient(3, "Gnägi", "Johannes", "28.03.1989", "male");
        patientList.add(hirschli);
        patientList.add(kleinSchaad);
        patientList.add(gnagJ);
        }
        listAdapter = new PatientAdapter(this, R.layout.row, patientList);
        listView.setAdapter( listAdapter );
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
