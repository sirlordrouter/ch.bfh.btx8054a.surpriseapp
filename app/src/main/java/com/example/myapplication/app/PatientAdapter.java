package com.example.myapplication.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Johannes on 25.05.14.
 */
public class PatientAdapter extends ArrayAdapter<Patient> {

    private List<Patient> patients = null;

    public PatientAdapter(Context context, int resource, List<Patient> objects) {
        super(context, resource, objects);
        patients = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.row, null);
        }
        Patient o = patients.get(position);
        if (o != null) {
            TextView tt = (TextView) v.findViewById(R.id.toptext);
            TextView bt = (TextView) v.findViewById(R.id.bottomtext);
            if (tt != null) {
                tt.setText("Name: "+o.getName() + ", " + "Firstname: "+ o.getFirstname());                            }
            if(bt != null){
                bt.setText(o.getBirthdate()+ ", " + o.getGender());
            }
        }
        return v;
    }

}
